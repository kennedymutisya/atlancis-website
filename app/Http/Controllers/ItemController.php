<?php

namespace App\Http\Controllers;

use App\Item;
use Carbon\Carbon;
use Carbon\Traits\Date;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function store(Request $request, Item $item)
    {
        $this->validate($request, [
            'item'       => ['required'],
            'date'       => ['required'],
            'weight'     => ['required'],
            'perishable' => ['required'],
            'pickupdate' => ['required'],
        ]);

        $nights = $this->getNightsBetweenDates($request);
        list($hours, $days) = $this->RemainingDaysandHours($request, $nights);

        $price = $this->getPrices($nights, $hours, $days, $request->perishable, $request->weight);

        $item->item = $request->item;
        $item->date = $request->date;
        $item->weight = $request->weight;
        $item->perishable = $request->perishable;
        $item->pickupdate = $request->pickupdate;
        $item->price = $price;
        $item->saveOrFail();

        collect($item)->merge(['price' => $price]);
        return response($item);
    }

    protected function getPrices($nights, $hours, $days, $perishable, $weight)
    {
        if ($perishable === 'perishable') {
            if ($weight >= 0 && $weight <= 3) {
                $price = ($hours * 25) + ($days * 150) + ($nights * 150);
            } elseif ($weight >= 4 && $weight <= 15) {
                $price = ($hours * 50) + ($days * 200) + ($nights * 200);
            } elseif ($weight >= 16 && $weight <= 25) {
                $price = ($hours * 60) + ($days * 250) + ($nights * 250);
            } elseif ($weight > 25) {
                $price = ($hours * 100) + ($days * 300) + ($nights * 300);
            } else {
                $price = ($hours * 100) + ($days * 300) + ($nights * 300);
            }
            return $price;
        } else {
            if ($weight >= 0 && $weight <= 3) {
                $price = ($hours * 25) + ($days * 150) + ($nights * 100);
            } elseif ($weight >= 4 && $weight <= 15) {
                $price = ($hours * 50) + ($days * 200) + ($nights * 150);
            } elseif ($weight >= 16 && $weight <= 25) {
                $price = ($hours * 60) + ($days * 250) + ($nights * 200);
            } elseif ($weight > 25) {
                $price = ($hours * 100) + ($days * 300) + ($nights * 250);
            } else {
                $price = ($hours * 100) + ($days * 300) + ($nights * 250);
            }
            return $price;
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Item $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Item $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        //
    }

    /**
     * @param Request $request
     */
    protected function getNightsBetweenDates(Request $request)
    {
        $start_ts = strtotime($request->date);
        $end_ts = strtotime($request->pickupdate);
        $diff = $end_ts - $start_ts;
        return round($diff / 86400);
    }

    /**
     * @param Request $request
     */
    protected function diffInHours(Request $request)
    {
        $startTime = Carbon::parse($request->date);
        $finishTime = Carbon::parse($request->pickupdate);

        return $finishTime->diffInHours($startTime);
    }

    /**
     * @param Request $request
     * @param float $nights
     * @return int
     */
    protected function RemainingDaysandHours(Request $request, float $nights)
    {
        $totalHours = $this->diffInHours($request);
        $totalRemainingHours = $totalHours - ($nights * 12);

        $leftover = $totalRemainingHours / 12;
        $RemainingHours = round($leftover - floor($leftover), 1) * 10;
        $fullDays = round($totalRemainingHours / 12, 0);
        return [$RemainingHours, $fullDays];
    }

}
