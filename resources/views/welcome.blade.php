<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Start of Fav icon code -->
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('front/assets/img/fav/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('front/assets/img/fav/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('front/assets/img/apple-icon-72x72.html') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('front/assets/img/fav/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('front/assets/img/fav/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('front/assets/img/fav/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('front/assets/img/fav/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('front/assets/img/fav/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('front/assets/img/fav/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"
          href="{{ asset('front/assets/img/fav/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('front/assets/img/fav/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('front/assets/img/fav/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('front/assets/img/fav/favicon-16x16.png') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="ms-icon-144x144.html">
    <meta name="theme-color" content="#ffffff">
    <!-- End of Fav icon code -->

    <!-- Start Of Phone Number Dropdown -->
    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">-->
    <link href="../cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/css/intlTelInput.css" rel="stylesheet"
          media="screen">
    <!-- End of Phone Number Dropdown -->

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:400,500,600">
    <link rel="stylesheet" href="{{ asset('public/assets/font-awesome/css/fontawesome-all.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/all.css') }}">
    <title>Atlancis Technologies Limited</title>
    <!--<script src='https://www.google.com/recaptcha/api.js'></script>-->
</head>
<body data-spy="scroll" data-target=".navbar" class="" data-bg-parallax="scroll"
      data-bg-parallax-speed="3">
<div class="ts-page-wrapper" id="page-top">
    <!--*********************************************************************************************************-->
    <!--************ HERO ***************************************************************************************-->
    <!--*********************************************************************************************************-->
    <header id="ts-hero" class="ts-full-screen">

        <!--NAVIGATION ******************************************************************************************-->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top ts-separate-bg-element" data-bg-color="#173158">
            <div class="container nave">
                <a class="navbar-brand" href="/">
                    <img class="img-fluid logo" src="{{ asset('front/assets/img/Atlancislogo1.png') }}" alt="">
                </a>
                <!--end navbar-brand-->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                        aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <!--end navbar-toggler-->
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div class="navbar-nav ml-auto text-right">
                        <a class="nav-item nav-link active ts-scroll" href="#what-we-do">Home <span class="sr-only">(current)</span></a>
                        <a class="nav-item nav-link ts-scroll" href="#what-we-do">About Us</a>
                        <a class="nav-item nav-link ts-scroll" href="#our-solutions">Our Solutions</a>
                        <a class="nav-item nav-link ts-scroll" href="#our-products">Our Products</a>
                        <a class="nav-item nav-link ts-scroll" href="#our-clients">Our Clients</a>
                        <a class="nav-item nav-link ts-scroll" href="#gallery">Products Gallery</a>
                        <!--<a class="nav-item nav-link ts-scroll" href="news">News & Insights</a>-->
                        <a class="nav-item nav-link ts-scroll" href="#form-contact">Contact Us</a>
                        <!--<a class="nav-item nav-link ts-scroll btn btn-primary btn-sm text-white ml-3 px-3 ts-width__auto" href="#download">Download</a>-->
                    </div>
                    <!--end navbar-nav-->
                </div>
                <!--end collapse-->
            </div>
            <!--end container-->
        </nav>
        <!--end navbar-->
        <!-- Kinetic HCM modal -->
        <div class="modal fade" id="kinetic" tabindex="-1" role="dialog" aria-labelledby="basicModal"
             aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header" data-bg-color="#BA2127">
                        <h2 class="text-center modal-title text-white" id="myModalLabel">Kinetic HCM</h2>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fas fa-times-circle"></i></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <!--<div class="ts-title text-white py-5 mb-0  text-center">-->
                        <h3 class="mb-0 font-weight-normal text-center">
                            <img src="{{ asset('front/assets/img/team/kinetic.png') }}" class="img-fluid text-center">
                        </h3>
                        <!--<small class="ts-opacity__50 text-center" style="color: #000000 !important;">Atlancis Cloud Services</small>-->
                        <!--</div>-->

                        <div class="container">
                            <div class="row">

                                <div class="col-md-12 col-xl-12 col-sm-12 text-center">
                                    <h3>Human Resource Management</h3>
                                    <p class="text-left" style="color: #000000;">
                                        Effective management of an organisations workforce is important if you are to
                                        derive
                                        maximum return, improve the teams capabilities and grow productivity across the
                                        enterprise.

                                        Kinetic HCM is a comprehensive web-based human resource management tool that
                                        transforms
                                        complicated manual human resource processes into a simplified automated system.
                                        It is
                                        intuitive and easy to use while integrating all Human Resource (HR) processes
                                        from
                                        recruitment to on-boarding and through to exit - leave, performance, expense and
                                        time
                                        management.

                                        Kinetic HCM is a cloud-based, global solution for the total management of any
                                        workforce.
                                        Being highly interactive, the system is designed to be simple and easy-to-use
                                        for a tool
                                        that is packed full of essential features.
                                    </p>
                                </div>
                                <!--</div>-->
                                <!--end col-xl-12-->
                            </div>
                            <!--end row-->
                        </div>
                        <!--end container-->

                    </div>
                    <div class="modal-footer" data-bg-color="#173158">
                        <button type="button" class="btn btn-default mr-auto" id="contact5" name="download_btn">Contact
                            Us <i
                                    class="far fa-arrow-alt-circle-right"></i></button>
                        <button type="button" class="btn btn-default text-white" data-dismiss="modal"
                                style="background-color: #BA2127;">Close <i class="far fa-times-circle"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Kinetic HCM Modal -->

        <!-- Atlancis Cloud modal -->
        <div class="modal fade" id="cloud" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header" data-bg-color="#BA2127">
                        <h2 class="text-center modal-title text-white" id="myModalLabel">Atlancis Cloud</h2>
                    <!--<figure class="position-absolute text-center w-100 ts-z-index__1 align-right"
                                data-animate="ts-zoomInShort">
                            <img src="{{ asset('front/assets/img/img-screen-small-01.png') }}" class="mw-100 d-inline-block ts-shadow__lg" alt="">
                        </figure>-->
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fas fa-times-circle"></i></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <!--<div class="ts-title text-white py-5 mb-0  text-center">-->
                        <h3 class="mb-0 font-weight-normal text-center">
                            <img src="{{ asset('front/assets/img/team/cloud.png') }}" class="img-fluid text-center">
                        </h3>
                        <!--<small class="ts-opacity__50 text-center" style="color: #000000 !important;">Atlancis Cloud Services</small>-->
                        <!--</div>-->

                        <div class="container">
                            <div class="row">

                                <div class="col-md-12 col-xl-12 col-sm-12 text-center">
                                    <h3>Atlancis Cloud Services</h3>
                                    <!--a href="https://cloud.atlancis.com"><p class="ts-opacity__50" style="color: #0d47a1 !important; font-size: 17px;">www.cloud.atlancis.com</p></a-->
                                    <p class="text-left" style="color: #000000;">
                                        Africa’s first True Cloud! Our Self Service cloud platform delivers the
                                        capabilities of
                                        world class cloud
                                        solutions to Africa, and hosted in Africa. Through our partner Tier 3+
                                        Datacentres in
                                        Kenya, Tanzania, Uganda,
                                        and counting, we bring cloud back! Our approach is to make our IaaS, SaaS and
                                        PaaS
                                        solution as close to you as
                                        possible whilst enabling you to choose which best fit model works for you –
                                        ATLANCIS
                                        Public Cloud, ATLANCIS
                                        Private Cloud or a Hybrid of the two. Our highly skilled engineers will be
                                        available to
                                        walk with you on this journey.<br>

                                    <ul class="produ" type="disc">
                                        <li class="text-left"><strong>SaaS</strong> - <span>Our SaaS offering will help you with key processes, including finance, sales and asset
                                management, allowing you to spend more time on innovation and less on IT.</span>
                                        </li>
                                        <li class="text-left"><strong>PaaS</strong> - <span>All the tools necessary to build cutting edge applications are available to you, allowing
                                        you to reduce the hassle, increase the speed from concept to deployment and reduce the costs associated with on-site infrastructure.</span>
                                        </li>
                                        <li class="text-left"><strong>IaaS</strong> - <span>Atlancis hosts the infrastructure and handles tasks like system maintenance and backups, so
                            you do not have to buy hardware or employ in-house experts to manage it. Our systems deliver
                            affordability, simplicity, availability and growth.</span>
                                        </li>
                                    </ul>
                                    </p>
                                </div>
                                <!--</div>-->
                                <!--end col-xl-12-->
                            </div>
                            <!--end row-->
                        </div>
                        <!--end container-->

                    </div>
                    <div class="modal-footer" data-bg-color="#173158">
                        <button type="button" class="btn btn-default mr-auto" id="contact6" name="download_btn">Contact
                            Us <i
                                    class="far fa-arrow-alt-circle-right"></i></button>
                        <button type="button" class="btn btn-default text-white" data-dismiss="modal"
                                style="background-color: #BA2127;">Close <i class="far fa-times-circle"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Atlancis Cloud Modal -->

        <!-- iLearn modal -->
        <div class="modal fade" id="ilearn" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header" data-bg-color="#BA2127">
                        <h2 class="text-center modal-title text-white" id="myModalLabel">iLearn</h2>
                    <!--<figure class="position-absolute text-center w-100 ts-z-index__1 align-right"
                                data-animate="ts-zoomInShort">
                            <img src="{{ asset('front/assets/img/img-screen-small-01.png') }}" class="mw-100 d-inline-block ts-shadow__lg" alt="">
                        </figure>-->
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fas fa-times-circle"></i></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <!--<div class="ts-title text-white py-5 mb-0  text-center">-->
                        <h3 class="mb-0 font-weight-normal text-center">
                            <img src="{{ asset('front/assets/img/team/iLearn.png') }}" class="img-fluid text-center">
                        </h3>
                        <!--<small class="ts-opacity__50 text-center" style="color: #000000 !important;">Atlancis Cloud Services</small>-->
                        <!--</div>-->

                        <div class="container">
                            <div class="row">

                                <div class="col-md-12 col-xl-12 col-sm-12 text-center">
                                    <h3>Learning Management System</h3>
                                    <p class="text-left" style="color: #000000;">
                                        ILearn is a Learning Management System that provides the dynamic capability to
                                        plan,
                                        deliver and analyze learning progress of learners by providing administration,
                                        documentation, tracking and analysis functions to meet learning outcomes. It has
                                        enabled
                                        an increase in creativity and flexibility that wasn’t available in a textbook
                                        centric
                                        learning environment.
                                    </p>
                                </div>
                                <!--</div>-->
                                <!--end col-xl-12-->
                            </div>
                            <!--end row-->
                        </div>
                        <!--end container-->

                    </div>
                    <div class="modal-footer" data-bg-color="#173158">
                        <button type="button" class="btn btn-default mr-auto" id="contact7" name="download_btn">Contact
                            Us <i
                                    class="far fa-arrow-alt-circle-right"></i></button>
                        <button type="button" class="btn btn-default text-white" data-dismiss="modal"
                                style="background-color: #BA2127;">Close <i class="far fa-times-circle"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- iLearn Modal -->

        <!-- Social Light modal -->
        <div class="modal fade" id="social" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header" data-bg-color="#BA2127">
                        <h2 class="text-center modal-title text-white" id="myModalLabel">SociaLight</h2>
                    <!--<figure class="position-absolute text-center w-100 ts-z-index__1 align-right"
                                data-animate="ts-zoomInShort">
                            <img src="{{ asset('front/assets/img/img-screen-small-01.png') }}" class="mw-100 d-inline-block ts-shadow__lg" alt="">
                        </figure>-->
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fas fa-times-circle"></i></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <!--<div class="ts-title text-white py-5 mb-0  text-center">-->
                        <h3 class="mb-0 font-weight-normal text-center">
                            <img src="{{ asset('front/assets/img/team/social.png') }}" class="img-fluid text-center">
                        </h3>
                        <!--<small class="ts-opacity__50 text-center" style="color: #000000 !important;">Atlancis Cloud Services</small>-->
                        <!--</div>-->

                        <div class="container">
                            <div class="row">

                                <div class="col-md-12 col-xl-12 col-sm-12 text-center">
                                    <h3>Social Media Analytics System</h3>
                                    <p class="text-left" style="color: #000000;">
                                        Socialight provides insights into the customer journey, improves customer
                                        services
                                        response times and give you a competitive edge by allowing hyper segmentation of
                                        audiences leading to campaign success.<br>

                                        Establishing and maintaining an online reputation is crucial for brand success.
                                        Socialight gives deep insights into your online presence enabling the
                                        development and
                                        execution of a clear strategy. Customised dashboards show your metrics and
                                        engagement
                                        across social media and website (rss) feeds both in real time and historically.

                                    <ul class="produ" type="disc">
                                        <h4 class="text-left">Key Features:</h4>
                                        <li class="text-left">
                                            Socialight provides real-time data mining.
                                        </li>
                                        <li class="text-left">
                                            Flexibility in report generation.
                                        </li>
                                        <li class="text-left">
                                            Analytics and forecasting using historical data.
                                        </li>
                                        <li class="text-left">
                                            A user-friendly interface and easy integration
                                            with other products.
                                        </li>
                                    </ul>
                                    </p>
                                </div>
                                <!--</div>-->
                                <!--end col-xl-12-->
                            </div>
                            <!--end row-->
                        </div>
                        <!--end container-->

                    </div>
                    <div class="modal-footer" data-bg-color="#173158">
                        <button type="button" class="btn btn-default mr-auto" id="contact8" name="download_btn">Contact
                            Us <i
                                    class="far fa-arrow-alt-circle-right"></i></button>
                        <button type="button" class="btn btn-default text-white" data-dismiss="modal"
                                style="background-color: #BA2127;">Close <i class="far fa-times-circle"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Social Light Modal -->

        <!-- Atlancis Leasing modal -->
        <div class="modal fade" id="leasing" tabindex="-1" role="dialog" aria-labelledby="basicModal"
             aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header" data-bg-color="#BA2127">
                        <h2 class="text-center modal-title text-white" id="myModalLabel">Atlancis Leasing</h2>
                    <!--<figure class="position-absolute text-center w-100 ts-z-index__1 align-right"
                                data-animate="ts-zoomInShort">
                            <img src="{{ asset('front/assets/img/img-screen-small-01.png') }}" class="mw-100 d-inline-block ts-shadow__lg" alt="">
                        </figure>-->
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fas fa-times-circle"></i></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <!--<div class="ts-title text-white py-5 mb-0  text-center">-->
                        <h3 class="mb-0 font-weight-normal text-center">
                            <img src="{{ asset('front/assets/img/team/leasing.png') }}" class="img-fluid text-center">
                        </h3>
                        <!--<small class="ts-opacity__50 text-center" style="color: #000000 !important;">Atlancis Cloud Services</small>-->
                        <!--</div>-->

                        <div class="container">
                            <div class="row">

                                <div class="col-md-12 col-xl-12 col-sm-12 text-center">
                                    <h3>Leasing System</h3>
                                    <p class="text-left" style="color: #000000;">
                                        Atlancis Leasing provides 100% of the capital needed to acquire equipment,
                                        software and
                                        services needed with a single, consistent, low monthly or quarterly
                                        payment.Leasing from
                                        us provides you with a number of critical advantages.

                                    <ul class="produ" type="disc">
                                        <h4 class="text-left">Atlancis Leasing Advantages:</h4>
                                        <li class="text-left"><strong>Tax advantages</strong> -
                                            <span>
                                        Leasing costs are treated as operational expenses (OpEx) and expensed in the accounting period which they occur.
                                    </span>
                                        </li>
                                        <li class="text-left"><strong>Predictability</strong> -
                                            <span>
                                        Businesses prefer the predictability of OpEx; if they can be covered by the run-rate, projects are more likely to be greenlighted.
                                    </span>
                                        </li>
                                        <li class="text-left"><strong>Lower capital costs</strong> -
                                            <span>
                                        Capital expenses (CapEx) are reduced as hardware purchases are no longer a requirement.
                                    </span>
                                        </li>
                                        <li class="text-left"><strong>No costly upgrades</strong> -
                                            <span>
                                        Leasing reduces the cost of purchasing new technology as well as the downtime that occurs during migration.
                                    </span>
                                        </li>
                                        <li class="text-left"><strong>Reducing labor costs</strong> -
                                            <span>
                                        Leasing allows you to outsource the cost of administering your infrastructure which reduces labor cost.
                                    </span>
                                        </li>
                                    </ul>
                                    </p>
                                </div>
                                <!--</div>-->
                                <!--end col-xl-12-->
                            </div>
                            <!--end row-->
                        </div>
                        <!--end container-->

                    </div>
                    <div class="modal-footer" data-bg-color="#173158">
                        <button type="button" class="btn btn-default mr-auto" id="contact9" name="download_btn">Contact
                            Us <i
                                    class="far fa-arrow-alt-circle-right"></i></button>
                        <button type="button" class="btn btn-default text-white" data-dismiss="modal"
                                style="background-color: #BA2127;">Close <i class="far fa-times-circle"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Atlancis Leasing Modal -->

        <!-- iQueue modal -->
        <div class="modal fade" id="iqueue" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header" data-bg-color="#BA2127">
                        <h2 class="text-center modal-title text-white" id="myModalLabel">iQueue</h2>
                    <!--<figure class="position-absolute text-center w-100 ts-z-index__1 align-right"
                                data-animate="ts-zoomInShort">
                            <img src="{{ asset('front/assets/img/img-screen-small-01.png') }}" class="mw-100 d-inline-block ts-shadow__lg" alt="">
                        </figure>-->
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fas fa-times-circle"></i></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <!--<div class="ts-title text-white py-5 mb-0  text-center">-->
                        <h3 class="mb-0 font-weight-normal text-center">
                            <img src="{{ asset('front/assets/img/team/iQueue.png') }}" class="img-fluid text-center">
                        </h3>
                        <!--<small class="ts-opacity__50 text-center" style="color: #000000 !important;">Atlancis Cloud Services</small>-->
                        <!--</div>-->

                        <div class="container">
                            <div class="row">

                                <div class="col-md-12 col-xl-12 col-sm-12 text-center">
                                    <h3>Queue Management System</h3>
                                    <p class="text-left" style="color: #000000;">
                                        The iQueue management system improves the customer experience while ensuring
                                        that
                                        staffing levels are optimised. The iQueue system can be adapted to any venue.
                                        This
                                        directly impacts costs and revenues, customer retention, and public brand image.
                                        Our
                                        solution will manage any incidents from the moment they are captured right
                                        through to
                                        their resolution.
                                    </p>
                                </div>
                                <!--</div>-->
                                <!--end col-xl-12-->
                            </div>
                            <!--end row-->
                        </div>
                        <!--end container-->

                    </div>
                    <div class="modal-footer" data-bg-color="#173158">
                        <button type="button" class="btn btn-default text-white" data-dismiss="modal"
                                style="background-color: #BA2127;">Close <i class="far fa-times-circle"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- iQueue Modal -->
        <!-- Networking Solutions modal -->
        <div class="modal fade" id="network" tabindex="-1" role="dialog" aria-labelledby="basicModal"
             aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header" data-bg-color="#BA2127">
                        <h2 class="text-center modal-title text-white" id="myModalLabel">Networking Solutions</h2>
                    <!--<figure class="position-absolute text-center w-100 ts-z-index__1 align-right"
                                data-animate="ts-zoomInShort">
                            <img src="{{ asset('front/assets/img/img-screen-small-01.png') }}" class="mw-100 d-inline-block ts-shadow__lg" alt="">
                        </figure>-->
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fas fa-times-circle"></i></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <!--<div class="ts-title text-white py-5 mb-0  text-center">-->
                        <h3 class="mb-0 font-weight-normal text-center">
                            <img src="{{ asset('front/assets/img/icons/network%20solutions1.png') }}"
                                 class="img-fluid text-center">
                        </h3>
                        <!--<small class="ts-opacity__50 text-center" style="color: #000000 !important;">Atlancis Cloud Services</small>-->
                        <!--</div>-->

                        <div class="container">
                            <div class="row">

                                <div class="col-md-12 col-xl-12 col-sm-12 text-center">
                                    <h3>Highly Reliable Networking Solutions</h3>
                                    <p class="text-left" style="color: #000000;">
                                        Atlancis is keen to ensure that your network infrastructure is stable and
                                        operates
                                        reliably. We will eliminate the burden of ongoing support, maintenance and the
                                        monitoring of your network infrastructure as well as offer routine configuration
                                        assistance and upgrades as an enhancement. Your network should support your
                                        business
                                        needs and Atlancis will ensure that your network can handle the capacity of the
                                        day to
                                        day operations of an office, onsite or in a remote location round the clock.
                                    </p>
                                    <ul class="list-inline partnerimg">
                                        <li class="list-inline-item"><img
                                                    src="{{ asset('front/assets/img/partners_icon/cisco.png') }}"></li>
                                        <li class="list-inline-item"><img
                                                    src="{{ asset('front/assets/img/partners_icon/Partners_64_642.png') }}">
                                        </li>
                                        <li class="list-inline-item"><img
                                                    src="{{ asset('front/assets/img/partners_icon/huawei.png') }}">
                                        </li>
                                    </ul>
                                </div>
                                <!--</div>-->
                                <!--end col-xl-12-->
                            </div>
                            <!--end row-->
                        </div>
                        <!--end container-->

                    </div>
                    <div class="modal-footer" data-bg-color="#173158">
                        <button type="button" class="btn btn-default mr-auto" id="contact" name="download_btn">Contact
                            Us <i
                                    class="far fa-arrow-alt-circle-right"></i></button>
                        <button type="button" class="btn btn-default text-white" data-dismiss="modal"
                                style="background-color: #BA2127;">Close <i class="far fa-times-circle"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Networking Solutions Modal -->

        <!-- Security Solutions modal -->
        <div class="modal fade" id="security" tabindex="-1" role="dialog" aria-labelledby="basicModal"
             aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header" data-bg-color="#BA2127">
                        <h2 class="text-center modal-title text-white" id="myModalLabel">Security Solutions</h2>
                    <!--<figure class="position-absolute text-center w-100 ts-z-index__1 align-right"
                                data-animate="ts-zoomInShort">
                            <img src="{{ asset('front/assets/img/img-screen-small-01.png') }}" class="mw-100 d-inline-block ts-shadow__lg" alt="">
                        </figure>-->
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fas fa-times-circle"></i></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <!--<div class="ts-title text-white py-5 mb-0  text-center">-->
                        <h3 class="mb-0 font-weight-normal text-center">
                            <img src="{{ asset('front/assets/img/icons/security1.png') }}"
                                 class="img-fluid text-center">
                        </h3>
                        <!--<small class="ts-opacity__50 text-center" style="color: #000000 !important;">Atlancis Cloud Services</small>-->
                        <!--</div>-->

                        <div class="container">
                            <div class="row">

                                <div class="col-md-12 col-xl-12 col-sm-12 text-center">
                                    <h3>Effective Security Solutions</h3>
                                    <p class="text-left" style="color: #000000;">
                                        At Atlancis, we put best-of-breed technologies into effect, so security becomes
                                        a
                                        business enabler — not an everyday interruption. Our portfolio embraces every
                                        facet of
                                        information security and risk management. By keeping the security solutions of
                                        your
                                        business up to date, you are not only protecting your most treasured assets, but
                                        also
                                        showing your clients and business partners that you care about their trust and
                                        privacy.
                                        Nothing is too small or too big for us.
                                    </p>
                                    <ul class="list-inline partnerimg">
                                        <li class="list-inline-item"><img
                                                    src="{{ asset('front/assets/img/partners_icon/fortinet.png') }}">
                                        </li>
                                        <li class="list-inline-item"><img
                                                    src="{{ asset('front/assets/img/partners_icon/Checkpoint_64.png') }}">
                                        </li>
                                        <li class="list-inline-item"><img
                                                    src="{{ asset('front/assets/img/partners_icon/Imperva_64.png') }}">
                                        </li>
                                    </ul>
                                </div>
                                <!--</div>-->
                                <!--end col-xl-12-->
                            </div>
                            <!--end row-->
                        </div>
                        <!--end container-->

                    </div>
                    <div class="modal-footer" data-bg-color="#173158">
                        <button type="button" class="btn btn-default mr-auto" id="contact1" name="download_btn">Contact
                            Us <i
                                    class="far fa-arrow-alt-circle-right"></i></button>
                        <button type="button" class="btn btn-default text-white" data-dismiss="modal"
                                style="background-color: #BA2127;">Close <i class="far fa-times-circle"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Security Solutions Modal -->

        <!-- Database Solutions modal -->
        <div class="modal fade" id="database" tabindex="-1" role="dialog" aria-labelledby="basicModal"
             aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header" data-bg-color="#BA2127">
                        <h2 class="text-center modal-title text-white" id="myModalLabel">Database Solutions</h2>
                    <!--<figure class="position-absolute text-center w-100 ts-z-index__1 align-right"
                                data-animate="ts-zoomInShort">
                            <img src="{{ asset('front/assets/img/img-screen-small-01.png') }}" class="mw-100 d-inline-block ts-shadow__lg" alt="">
                        </figure>-->
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fas fa-times-circle"></i></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <!--<div class="ts-title text-white py-5 mb-0  text-center">-->
                        <h3 class="mb-0 font-weight-normal text-center">
                            <img src="{{ asset('front/assets/img/icons/database1.png') }}"
                                 class="img-fluid text-center">
                        </h3>
                        <!--<small class="ts-opacity__50 text-center" style="color: #000000 !important;">Atlancis Cloud Services</small>-->
                        <!--</div>-->

                        <div class="container">
                            <div class="row">

                                <div class="col-md-12 col-xl-12 col-sm-12 text-center">
                                    <h3>Efficient Database Solutions</h3>
                                    <p class="text-left" style="color: #000000;">
                                        As data keeps growing, the pressure intensifies on performance and database
                                        administrators managing workloads in-house. Oracle Database as a Service (DBaaS)
                                        from
                                        Atlancis enables the transition of challenging workloads and reduces the
                                        pressure off
                                        your database management team. As an Oracle Gold partner, we have been growing
                                        steadily
                                        and our solutions have left a significant mark on some of the core ICT
                                        transformations
                                        in the region, earning us immense recognition.
                                    </p>
                                    <ul class="list-inline partnerimg">
                                        <li class="list-inline-item"><img
                                                    src="{{ asset('front/assets/img/partners_icon/oracle.png') }}">
                                        </li>
                                        <li class="list-inline-item"><img
                                                    src="{{ asset('front/assets/img/partners_icon/microsoft.png') }}">
                                        </li>
                                        <li class="list-inline-item"><img
                                                    src="{{ asset('front/assets/img/partners_icon/ibm.png') }}"></li>
                                        <li class="list-inline-item"><img
                                                    src="{{ asset('front/assets/img/partners_icon/sap.png') }}"></li>
                                        <li class="list-inline-item"><img
                                                    src="{{ asset('front/assets/img/partners_icon/cloud.png') }}"></li>
                                    </ul>
                                </div>
                                <!--</div>-->
                                <!--end col-xl-12-->
                            </div>
                            <!--end row-->
                        </div>
                        <!--end container-->

                    </div>
                    <div class="modal-footer" data-bg-color="#173158">
                        <button type="button" class="btn btn-default mr-auto" id="contact2" name="download_btn">Contact
                            Us <i
                                    class="far fa-arrow-alt-circle-right"></i></button>
                        <button type="button" class="btn btn-default text-white" data-dismiss="modal"
                                style="background-color: #BA2127;">Close <i class="far fa-times-circle"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Database Solutions Modal -->

        <!-- Enterprise Servers modal -->
        <div class="modal fade" id="servers" tabindex="-1" role="dialog" aria-labelledby="basicModal"
             aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header" data-bg-color="#BA2127">
                        <h2 class="text-center modal-title text-white" id="myModalLabel">Enterprise Servers</h2>
                    <!--<figure class="position-absolute text-center w-100 ts-z-index__1 align-right"
                                data-animate="ts-zoomInShort">
                            <img src="{{ asset('front/assets/img/img-screen-small-01.png') }}" class="mw-100 d-inline-block ts-shadow__lg" alt="">
                        </figure>-->
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fas fa-times-circle"></i></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <!--<div class="ts-title text-white py-5 mb-0  text-center">-->
                        <h3 class="mb-0 font-weight-normal text-center">
                            <img src="{{ asset('front/assets/img/icons/enterprise%20servers1.png') }}"
                                 class="img-fluid text-center">
                        </h3>
                        <!--<small class="ts-opacity__50 text-center" style="color: #000000 !important;">Atlancis Cloud Services</small>-->
                        <!--</div>-->

                        <div class="container">
                            <div class="row">

                                <div class="col-md-12 col-xl-12 col-sm-12 text-center">
                                    <h3>Scalable Enterprise Servers</h3>
                                    <p class="text-left" style="color: #000000;">
                                        Atlancis is setting the pace by drawing upon a deep expertise in providing
                                        superior data
                                        center solutions based on industry best practices, offering you a combination of
                                        services, software and products that result in optimized infrastructures,
                                        allowing you
                                        to focus on being more responsive, improve operational efficiency, and increase
                                        your
                                        organization’s strategic relevance to the business.
                                    </p>
                                    <ul class="list-inline partnerimg">
                                        <li class="list-inline-item"><img
                                                    src="{{ asset('front/assets/img/partners_icon/ibm.png') }}"></li>
                                        <li class="list-inline-item"><img
                                                    src="{{ asset('front/assets/img/partners_icon/huawei.png') }}">
                                        </li>
                                        <li class="list-inline-item"><img
                                                    src="{{ asset('front/assets/img/partners_icon/cisco.png') }}"></li>
                                        <li class="list-inline-item"><img
                                                    src="{{ asset('front/assets/img/partners_icon/dell.png') }}"></li>
                                        <li class="list-inline-item"><img
                                                    src="{{ asset('front/assets/img/partners_icon/oracle.png') }}">
                                        </li>
                                        <li class="list-inline-item"><img
                                                    src="{{ asset('front/assets/img/partners_icon/cloud.png') }}"></li>
                                    </ul>
                                </div>
                                <!--</div>-->
                                <!--end col-xl-12-->
                            </div>
                            <!--end row-->
                        </div>
                        <!--end container-->

                    </div>
                    <div class="modal-footer" data-bg-color="#173158">
                        <button type="button" class="btn btn-default mr-auto" id="contact3" name="download_btn">Contact
                            Us <i
                                    class="far fa-arrow-alt-circle-right"></i></button>
                        <button type="button" class="btn btn-default text-white" data-dismiss="modal"
                                style="background-color: #BA2127;">Close <i class="far fa-times-circle"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Enterprise Servers Modal -->

        <!-- Enterprise Applications Solutions modal -->
        <div class="modal fade" id="applications" tabindex="-1" role="dialog" aria-labelledby="basicModal"
             aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header" data-bg-color="#BA2127">
                        <h2 class="text-center modal-title text-white" id="myModalLabel">Enterprise Applications
                            Solutions</h2>
                    <!--<figure class="position-absolute text-center w-100 ts-z-index__1 align-right"
                                data-animate="ts-zoomInShort">
                            <img src="{{ asset('front/assets/img/img-screen-small-01.png') }}" class="mw-100 d-inline-block ts-shadow__lg" alt="">
                        </figure>-->
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fas fa-times-circle"></i></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <!--<div class="ts-title text-white py-5 mb-0  text-center">-->
                        <h3 class="mb-0 font-weight-normal text-center">
                            <img src="{{ asset('front/assets/img/icons/enterprise%20applications1.png') }}"
                                 class="img-fluid text-center">
                        </h3>
                        <!--<small class="ts-opacity__50 text-center" style="color: #000000 !important;">Atlancis Cloud Services</small>-->
                        <!--</div>-->

                        <div class="container">
                            <div class="row">

                                <div class="col-md-12 col-xl-12 col-sm-12 text-center">
                                    <h3>Fully Functional & Responsive Enterprise Applications Solutions</h3>
                                    <p class="text-left" style="color: #000000;">
                                        Access to enterprise applications round the clock is crucial for key business
                                        processes.
                                        Our applications are internally developed; from CRM and ERP systems as well as
                                        Business
                                        Intelligence, Atlancis has expert teams that can design, implement, manage and
                                        support
                                        all major applications that support your business.
                                    </p>
                                    <ul class="list-inline partnerimg">
                                        <li class="list-inline-item"><img
                                                    src="{{ asset('front/assets/img/partners_icon/oracle.png') }}">
                                        </li>
                                        <li class="list-inline-item"><img
                                                    src="{{ asset('front/assets/img/partners_icon/cloud.png') }}"></li>
                                    </ul>
                                </div>
                                <!--</div>-->
                                <!--end col-xl-12-->
                            </div>
                            <!--end row-->
                        </div>
                        <!--end container-->

                    </div>
                    <div class="modal-footer" data-bg-color="#173158">
                        <button type="button" class="btn btn-default mr-auto" id="contact4" name="download_btn">Contact
                            Us <i
                                    class="far fa-arrow-alt-circle-right"></i></button>
                        <button type="button" class="btn btn-default text-white" data-dismiss="modal"
                                style="background-color: #BA2127;">Close <i class="far fa-times-circle"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Enterprise Applications Solutions Modal -->


        <!-- Start of Download Atlancis Profile Modal -->
        <div class="modal fade" data-backdrop="static" id="download" tabindex="-1" role="dialog"
             aria-labelledby="basicModal"
             aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header" data-bg-color="#BA2127">
                        <h4 class="modal-title" id="myModalLabel">Download Atlancis Company Profile</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fas fa-times-circle"></i></span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <form id="modal-subscribe" method="POST"
                              class="clearfix ts-form ts-form-email ts-inputs__transparent"
                              action="https://www.atlancis.com/index.php" enctype="multipart/form-data"
                              data-php-path="assets/php/email.html">
                            <div class="row">
                                <div class="col-md-8 col-sm-8">
                                    <div class="form-group">
                                        <label for="form-subscribe-email">Your Email <span class="text-danger">*</span></label>
                                        <input type="email" class="form-control" id="form-subscribe-email"
                                               name="email_subscribe" placeholder="Your Email" required>
                                    </div>
                                    <!--end form-group -->
                                </div>
                                <!--end col-md-6 col-sm-6 -->
                            </div>
                            <div class="container">
                                <!--end row -->
                                <div class="row">
                                    <div class="col-md-8 col-sm-8 offset">
                                        <div class="form-group">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="form-check-input" name="check"
                                                       checked="checked">
                                                Subscribe to our newsletter
                                            </label>
                                            <p>Sign up here to get the latest news, updates and special offers delivered
                                                directly to your inbox.</p>
                                        </div>
                                        <!--end form-group -->
                                    </div>
                                    <!--end col-md-6 col-sm-6 -->
                                </div>
                                <!--end row -->
                            </div>
                            <div class="form-group clearfix">
                                <button type="submit" class="btn btn-default float-left" id="download_btn"
                                        name="download_btn">
                                    Download <i class="fas fa-download"></i></button>
                            </div>
                            <!--end form-group -->
                            <div class="form-contact-status"></div>
                        </form>
                        <!--end form-contact -->
                    </div>
                    <!--<div class="modal-footer" data-bg-color="#173158">
                        <button type="button" class="btn btn-default text-white" data-dismiss="modal"
                                style="background-color: #BA2127;">Close <i class="far fa-times-circle"></i></button>
                    </div>-->
                </div>
            </div>
        </div>
        <!-- End of Download Atlancis Profile Modal -->
        <!--HERO CONTENT ****************************************************************************************-->
        <div class="container align-self-center"
             style="padding-top: 0 !important; margin-top:0 !important; margin-bottom: 10px !important; padding-bottom: 10px !important;">
            <div class="init row align-items-center">
                <div class="col-sm-7 intro float-right">
                    <!--<div class="mx-auto d-block text-center">
                        <i class=""></i>
                        <i class="fa fa-4x fas fa-laptop fa-spin" style="font-size: 100px;"></i>
                    </div><br>-->
                    <!--<h3 class="text-white"><strong>Welcome to Atlancis Technologies Limited!</strong></h3>-->
                    <h1 class="text-white bounceInDown slogan">Making IT Simple</h1>
                    <!--<h3 class="text-white"><strong>Solutions that work for you</strong></h3>-->
                    <p class="text-white" style="font-size: 18px;">Our Innovative solutions enable you to optimize,
                        secure,
                        manage and
                        support your mission-critical infrastructure.
                        We are a creative technology company providing
                        key digital services.</p>
                    <a href="about.html" class="btn btn-light btn-lg ts-scroll">Learn More <i
                                class="fas fa-angle-double-right"></i></a>
                </div>
                <!--end col-sm-7 col-md-7-->
                <div class="col-sm-5 d-none d-sm-block float-left">
                    <div class="owl-carousel text-center zoom" data-owl-nav="1" data-owl-loop="1">
                        <img src="{{ asset('front/assets/img/MacBook_7.png') }}"
                             class="d-inline-block mw-100 p-md-5 p-lg-0 ts-width__auto"
                             alt="">
                        <img src="{{ asset('front/assets/img/MacBook_5.png') }}"
                             class="d-inline-block mw-100 p-md-5 p-lg-0 ts-width__auto"
                             alt="">
                        <img src="{{ asset('front/assets/img/MacBook_1.png') }}"
                             class="d-inline-block mw-100 p-md-5 p-lg-0 ts-width__auto"
                             alt="">
                        <img src="{{ asset('front/assets/img/MacBook_2.png') }}"
                             class="d-inline-block mw-100 p-md-5 p-lg-0 ts-width__auto"
                             alt="">
                        <img src="{{ asset('front/assets/img/MacBook_3.png') }}"
                             class="d-inline-block mw-100 p-md-5 p-lg-0 ts-width__auto"
                             alt="">
                        <img src="{{ asset('front/assets/img/MacBook_6.png') }}"
                             class="d-inline-block mw-100 p-md-5 p-lg-0 ts-width__auto"
                             alt="">
                        <img src="{{ asset('front/assets/img/MacBook_4.png') }}"
                             class="d-inline-block mw-100 p-md-5 p-lg-0 ts-width__auto"
                             alt="">
                        <img src="{{ asset('front/assets/img/Macbook_ilearn_1.png') }}"
                             class="d-inline-block mw-100 p-md-5 p-lg-0 ts-width__auto"
                             alt="">
                        <img src="{{ asset('front/assets/img/Macbook_ilearn_2.png') }}"
                             class="d-inline-block mw-100 p-md-5 p-lg-0 ts-width__auto"
                             alt="">
                    </div>
                </div>
                <!--end col-sm-5 col-md-5 col-xl-5-->
            </div>
            <!--end row-->
        </div>
        <!--end container-->

        <div id="ts-dynamic-waves" class="ts-background intr" data-bg-color="#0D4F8B">
            <!--<svg class="ts-svg ts-parallax-element" width="100%" height="100%" version="1.1"
                 xmlns="http://www.w3.org/2000/svg">
                <defs></defs>
                <path class="ts-dynamic-wave" d="" data-wave-color="#0D4F8B" data-wave-height=".6" data-wave-bones="4"
                      data-wave-speed="0.15"/>
            </svg>
            <svg class="ts-svg" width="100%" height="100%" version="1.1" xmlns="http://www.w3.org/2000/svg">
                <defs></defs>
                <path class="ts-dynamic-wave" d="" data-wave-color="#fff" data-wave-height=".2" data-wave-bones="6"
                      data-wave-speed="0.2"/>
            </svg>-->
        </div>

    </header>
    <!--end #hero-->

    <!--*********************************************************************************************************-->
    <!--************ CONTENT ************************************************************************************-->
    <!--*********************************************************************************************************-->
    <main id="ts-content">

        <!--WHAT WE DO ************************************************************************************-->
        <section id="what-we-do" class="ts-block"
                 style="margin-bottom: 5px !important; padding-bottom: 5px !important;">
            <div class="container">
                <div class="ts-title">
                    <h2>What We Do</h2>
                </div>
                <!--end ts-title-->
                <div class="row">
                    <div class="col-md-5 col-xl-5 col-sm-12" data-animate="ts-fadeInUp" data-offset="100">
                        <div class="clearfix margin-bottom-30">
                            <i class="fas fa-trophy icon-clear"></i>
                            <div class="content-boxes-in-v3">
                                <h5 class="heading-sm">INFRASTRUCTURE</h5>
                                <p>We have the technical capability to scope and deliver the best setup for your
                                    specific needs while providing your team with the right tools to monitor and manage
                                    the network.</p>
                            </div>
                        </div>
                        <div class="clearfix margin-bottom-30">
                            <i class="fas fa-expand-arrows-alt icon-clear"></i>
                            <div class="content-boxes-in-v3">
                                <h5 class="heading-sm">Software
                                    Solutions</h5>
                                <p>We have the expertise and certification to deploy the full suite of critical
                                    business applications and databases.</p>
                            </div>
                        </div>
                        <div class="clearfix margin-bottom-30">
                            <i class="fas fa-gem icon-clear"></i>
                            <div class="content-boxes-in-v3">
                                <h5 class="heading-sm">CONSULTING</h5>
                                <p>Atlancis has experience in deploying robust & scalable Enterprise Resource Planning
                                    (ERP) & Customer Relationship Management solutions to help you increase
                                    productivity,
                                    improve controls & deliver a seamless customer experience.</p>
                            </div>
                        </div>
                        <a href="about.html" class="btn btn-light btn-lg ts-scroll content-boxes-in-v3 text-white"
                           style="background-color: #d50000;">Learn More <i class="fas fa-angle-double-right"></i></a>
                    </div>
                    <!--end col-xl-5-->
                    <div class="col-md-7 col-xl-7 col-sm-12 text-center" data-animate="ts-fadeInUp" data-delay="0.1s"
                         data-offset="100">
                        <div class="px-3">
                        <!--<img src="{{ asset('front/assets/img/img-screen-desktop.jpg') }}" class="mw-100 ts-shadow__lg ts-border-radius__md" alt="">-->
                            <!-- Atlancis Video -->
                            <div class="embed-responsive embed-responsive-16by9 ts-shadow__lg ts-border-radius__md">
                                <video width="320" height="270" controls controlsList="nodownload">
                                    <source src="assets/img/Atlancis%20Technologies%20Intro%20updated%20edit%201.mp4"
                                            type="video/mp4">
                                </video>
                            </div>
                            <!-- End of Atlancis Video -->
                        </div>
                    </div>
                    <!--end col-xl-7-->
                </div>
                <!--end row-->
            </div>
            <!--end container-->
        </section>
        <!--END WHAT WE DO ********************************************************************************-->

        <!--WHO WE ARE ****************************************************************************************-->
        <section id="how-it-works" class="ts-block text-center"
                 style="margin-top: 5px !important; padding-top: 5px !important;">
            <div class="container">
                <div class="ts-title mid">
                    <h2 class="head">Who We Are</h2>
                </div>
                <!--end ts-title-->
                <div class="row">
                    <div class="col-sm-6 col-md-4 col-xl-4">
                        <figure data-animate="ts-fadeInUp">
                            <figure class="icon mb-5 p-2">
                                <img src="{{ asset('front/assets/img/about_us1.png') }}" alt="">
                                <!--<div class="ts-svg ts-svg__organic-shape-01" data-animate="ts-zoomInShort"></div>-->
                            </figure>
                            <h4>Our core values</h4>
                            <p>
                                Accountability, competence, support services, teamwork, agility, pride in peoples.
                            </p>
                        </figure>
                    </div>
                    <!--end col-xl-4-->
                    <div class="col-sm-6 col-md-4 col-xl-4">
                        <figure data-animate="ts-fadeInUp" data-delay="0.1s">
                            <figure class="icon mb-5 p-2">
                                <img src="{{ asset('front/assets/img/vision1.png') }}" alt="">
                                <!--<div class="ts-svg ts-svg__organic-shape-02" data-animate="ts-zoomInShort"></div>-->
                            </figure>
                            <h4>Our Vision</h4>
                            <p>
                                To be the market leading ICT solutions provider in Africa.
                            </p>
                        </figure>
                    </div>
                    <!--end col-xl-4-->
                    <div class="col-sm-6 offset-sm-4 col-md-4 offset-md-0 col-xl-4">
                        <figure data-animate="ts-fadeInUp" data-delay="0.2s">
                            <figure class="icon mb-5 p-2">
                                <img src="{{ asset('front/assets/img/mission1.png') }}" alt="">
                                <!--<div class="ts-svg ts-svg__organic-shape-03" data-animate="ts-zoomInShort"></div>-->
                            </figure>
                            <h4>Our Mission</h4>
                            <p>
                                Our mission is to partner actively with customers to provide innovative and sustainable
                                solutions to enhance business value.
                            </p>
                        </figure>
                    </div>
                    <!--end col-xl-4-->
                </div>
                <!--end row-->
            </div>
            <!--end container-->
        </section>
        <!--END OF WHO WE ARE ************************************************************************************-->

        <!--ATLANCIS DOCUMENT ********************************************************************************************-->
        <section id="profile">
            <div class="container">
                <div class="row">

                    <div class="col-md-7 offset-md-4">
                        <h4 class="mb-2" style="color: #173158 !important;">Atlancis Technologies Limited Profile</h4>
                        <div class="col-md-7 offset-md-1">
                            <button class="btn btn-info center text-center" data-toggle="modal" data-target="#download">
                                Download Atlancis Profile <i class="fas fa-download"></i></button>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <br/>
        <!--END ATLANCIS DOCUMENT ********************************************************************************************-->

        <!-- PARTNERS IMAGE SEPARATOR ********************************************************************************************-->
        <section id="img" class="py-5" data-bg-color="#f6f6f6"
                 data-bg-image="{{ asset('front/assets/img/partners1.jpg') }}">
            <!--end logos-->
        </section>
    </main>
</div>
<!--end container-->
<!-- END OF PARTNERS IMAGE SEPARATOR ****************************************************************************************-->

<!--PARTNERS ********************************************************************************************-->
<section id="partners" class="py-5 ts-block part" data-bg-color="#f6f6f6"
>
    <!--container-->
    <div class="container">
        <!--block of logos-->
        <div class="ts-title" style="padding-bottom: 0px; margin-bottom: 0px;">
            <h2>Our Partners</h2>
        </div>
        <div class="d-block justify-content-between align-items-center text-center ts-partners customer-logos">
            <a class="slide" href="#">
                <img src="{{ asset('front/assets/img/partners/Partners.jpg') }}" alt="">
            </a>
            <a class="slide" href="#">
                <img src="{{ asset('front/assets/img/partners/sap.jpg') }}" alt="">
            </a>
            <a class="slide" href="#">
                <img src="{{ asset('front/assets/img/partners/Partners3.jpg') }}" alt="">
            </a>
            <a class="slide" href="#">
                <img src="{{ asset('front/assets/img/partners/Partners4.jpg') }}" alt="">
            </a>
            <a class="slide" href="#">
                <img src="{{ asset('front/assets/img/partners/Partners5.png') }}" alt="">
            </a>
            <a class="slide" href="#">
                <img src="{{ asset('front/assets/img/partners/Partners6.jpg') }}" alt="">
            </a>
            <a class="slide" href="#">
                <img src="{{ asset('front/assets/img/partners/Partners7.jpg') }}" alt="">
            </a>
            <a class="slide" href="#">
                <img src="{{ asset('front/assets/img/partners/Partners8.jpg') }}" alt="">
            </a>
            <a class="slide" href="#">
                <img src="{{ asset('front/assets/img/partners/Partners9.jpg') }}" alt="">
            </a>
            <a class="slide" href="#">
                <img src="{{ asset('front/assets/img/partners/Partners10.jpg') }}" alt="">
            </a>
            <a class="slide" href="#">
                <img src="{{ asset('front/assets/img/partners/Partners11.jpg') }}" alt="">
            </a>
            <a class="slide" href="#">
                <img src="{{ asset('front/assets/img/partners/Partners12.jpg') }}" alt="">
            </a>
        </div>
        <!--end logos-->
    </div>
    <!--end container-->
</section>
<!--END PARTNERS ****************************************************************************************-->

<!--HOW OUR WORK LOOKS ****************************************************************************************-->
<section id="our-solutions" class="pb-0 ts-block text-center ts-overflow__hidden ts-shape-mask__down"
         data-bg-color="#ededed" data-bg-image="assets/img/" data-bg-repeat="no-repeat"
         data-bg-position="bottom" data-bg-size="inherit">
    <div class="container prod">
        <div class="ts-title mid" style="padding-bottom: 5px; margin-bottom: 5px;">
            <h2 class="quality">Our Solutions</h2>
        </div>
        <!--end ts-title-->
        <h5>We Build High Quality Solutions. <br>
            The 21st century dictates that business and technology blend seamlessly.
        </h5>

        <ul class="nav nav-tabs justify-content-center my-5" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="desktop-tab" data-toggle="tab" href="#desktop" role="tab"
                   aria-controls="desktop" aria-selected="true">
                    <h4>Desktop</h4>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="tablet-tab" data-toggle="tab" href="#tablet" role="tab"
                   aria-controls="tablet" aria-selected="false">
                    <h4>Tablet</h4>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="mobile-tab" data-toggle="tab" href="#mobile" role="tab"
                   aria-controls="mobile" aria-selected="false">
                    <h4>Mobile</h4>
                </a>
            </li>
        </ul>

        <div class="tab-content pt-5 ts-tabs-presentation" id="myTabContent" data-animate="ts-fadeInUp">
            <div class="tab-pane fade show active" id="desktop" role="tabpanel" aria-labelledby="desktop">
                <img src="{{ asset('front/assets/img/img-desktop.png') }}" class="mw-100" alt="">
            </div>
            <div class="tab-pane fade" id="tablet" role="tabpanel" aria-labelledby="tablet">
                <img src="{{ asset('front/assets/img/img-tablet.png') }}" class="mw-100" alt="">
            </div>
            <div class="tab-pane fade" id="mobile" role="tabpanel" aria-labelledby="mobile">
                <img src="{{ asset('front/assets/img/img-phone.png') }}" class="mw-100" alt="">
            </div>
        </div>

    </div>
</section>
<!--END OF HOW OUR WORK LOOKS************************************************************************************-->

<!--FEATURED SOLUTIONS ********************************************************************************************-->
<section id="features" class="ts-block" data-bg-image="assets/img/" data-bg-size="inherit"
         data-bg-position="left" data-bg-repeat="no-repeat">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-xl-7 text-center">
                <div class="position-relative">
                <!--<figure class="position-absolute text-center w-100 ts-z-index__1"
                            data-animate="ts-zoomInShort">
                        <img src="{{ asset('front/assets/img/img-screen-small-01.png') }}"
                             class="mw-100 d-inline-block ts-shadow__lg" alt="">
                    </figure>-->
                    <figure class="p-5 rounded" data-animate="ts-zoomInShort" data-delay="0.1s">
                        <img src="{{ asset('front/assets/img/sol.png') }}" class="mw-100 ts-shadow__lg" alt="">
                    </figure>
                <!--<figure class="position-absolute ts-bottom__0 ts-left__0 ts-z-index__2"
                            data-animate="ts-zoomInShort" data-delay="0.2s">
                        <img src="{{ asset('front/assets/img/img-screen-small-02.png') }}"
                             class="mw-100 d-inline-block ts-shadow__lg" alt="">
                    </figure>-->
                </div>
            </div>
            <!--end col-xl-7-->
            <div class="col-md-5 col-xl-5" data-animate="ts-fadeInUp" data-offset="100">
                <div class="ts-title" style="padding-bottom: 5px; margin-bottom: 5px;">
                    <h2>Quality Solutions</h2>
                </div>
                <!--end ts-title-->
                <p>
                    We make it easier and more affordable to enter and excel in high-growth
                    technology and vertical markets locally and around the world. Let our
                    technology experts, strategic alliances, training, resources and services
                    help you provide complete customer solutions that span the data center and IT lifecycle.
                </p>
                <ul type="disc">
                    <li>Speed time to market</li>
                    <li>Increase sales and profits</li>
                    <li>Minimize investment and risk</li>
                </ul>
            </div>
            <!--end col-xl-5-->
        </div>
        <!--end row-->

        <!-- Start row -->
        <div class="row" style="padding: 0 !important; margin: 0 !important;">
            <!-- col Network Solutions -->
            <div class="col-sm-12 col-md-4 shadow px-3 mb-5 bg-white rounded ts-font-color__black">
                <h4 class="text-dark text-center my-2">Network Solutions</h4>
                <div class="ts-shadow__lg ts-border-radius__md bg-white rounded">
                    <a class="popup-text" href="#1">
                        <img class="img-fluid mx-auto d-block"
                             src="{{ asset('front/assets/img/icons/network%20solutions1.png') }}"><i
                                class="fa fa-newspaper-o" aria-hidden="true"></i>
                    </a>
                    <div id="1" class="mfp-hide white-popup-block popup-text text-center">
                        <p class="solut">
                            Atlancis is keen to ensure that your network infrastructure is stable and
                            operates reliably.
                            We will eliminate the burden of ongoing support, maintenance and the monitoring
                            of your network
                            infrastructure as well as offer routine configuration assistance and upgrades as
                            an enhancement.
                        </p>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#network">Learn
                            More <i class="fas fa-angle-double-right"></i></button>
                        <div class="clearfix"></div>
                        <br/>
                    </div>
                </div>
            </div>
            <!-- End of col -->

            <!-- col Security Solutions -->
            <div class="col-sm-12 col-md-4 shadow px-3 mb-5 bg-white rounded ts-font-color__black">
                <h4 class="text-dark text-center my-2">Security Solutions</h4>
                <div class="ts-shadow__lg ts-border-radius__md bg-white rounded">
                    <a class="popup-text" href="#1">
                        <img class="img-fluid mx-auto d-block"
                             src="{{ asset('front/assets/img/icons/security1.png') }}"><i
                                class="fa fa-newspaper-o" aria-hidden="true"></i>
                    </a>
                    <div id="1" class="mfp-hide white-popup-block popup-text text-center">
                        <p class="solut">
                            We put best-of-breed technologies into effect, so security becomes a business
                            enabler — not an everyday interruption. Our portfolio embraces every facet of
                            information security and risk management. By keeping the security solutions of your
                            business current, you are showing your clients you care about their privacy.
                        </p>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#security">Learn
                            More <i class="fas fa-angle-double-right"></i></button>
                        <div class="clearfix"></div>
                        <br/>
                    </div>
                </div>
            </div>
            <!-- End of col -->

            <!-- col Database Solutions -->
            <div class="col-sm-12 col-md-4 shadow px-3 mb-5 bg-white rounded ts-font-color__black">
                <h4 class="text-dark text-center my-2">Database Solutions</h4>
                <div class="ts-shadow__lg ts-border-radius__md bg-white rounded">
                    <a class="popup-text" href="#1">
                        <img class="img-fluid mx-auto d-block"
                             src="{{ asset('front/assets/img/icons/database1.png') }}"><i
                                class="fa fa-newspaper-o" aria-hidden="true"></i>
                    </a>
                    <div id="1" class="mfp-hide white-popup-block popup-text text-center">
                        <p class="solut">
                            As data keeps growing, the pressure intensifies on performance and database
                            administrators managing workloads in-house.
                            Oracle Database as a Service (DBaaS) from Atlancis enables the transition of
                            challenging workloads and reduces the
                            pressure off your database management team.
                        </p>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#database">Learn
                            More <i class="fas fa-angle-double-right"></i></button>
                        <div class="clearfix"></div>
                        <br/>
                    </div>
                </div>
            </div>
            <!-- End of col -->
        </div>
        <!-- End row -->

        <!-- Start row -->
        <div class="row" style="padding-top: 0 !important; margin: 0 !important;">
            <!-- col Enterprise Servers Solutions -->
            <div class="col-sm-12 col-md-6 shadow px-3 mb-5 bg-white rounded ts-font-color__black">
                <h4 class="text-dark text-center my-2">Enterprise Servers</h4>
                <div class="ts-shadow__lg ts-border-radius__md bg-white rounded">
                    <a class="popup-text" href="#1">
                        <img class="img-fluid mx-auto d-block"
                             src="{{ asset('front/assets/img/icons/enterprise%20servers1.png') }}"><i
                                class="fa fa-newspaper-o" aria-hidden="true"></i>
                    </a>
                    <div id="1" class="mfp-hide white-popup-block popup-text text-center">
                        <p class="solut">
                            Atlancis is setting the pace by drawing upon a deep expertise in providing superior
                            data center solutions based on industry best practices, offering a combination of
                            services, software and products that result in optimized infrastructures, allowing
                            you to focus on improving operational efficiency.
                        </p>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#servers">Learn
                            More <i class="fas fa-angle-double-right"></i></button>
                        <div class="clearfix"></div>
                        <br/>
                    </div>
                </div>
            </div>
            <!-- End of col -->

            <!-- col Enterprise Applications Solutions -->
            <div class="col-sm-12 col-md-6 shadow px-3 mb-5 bg-white rounded ts-font-color__black">
                <h4 class="text-dark text-center my-2">Enterprise Applications</h4>
                <div class="ts-shadow__lg ts-border-radius__md bg-white rounded">
                    <a class="popup-text" href="#1">
                        <img class="img-fluid mx-auto d-block"
                             src="{{ asset('front/assets/img/icons/enterprise%20applications1.png') }}"><i
                                class="fa fa-newspaper-o" aria-hidden="true"></i>
                    </a>
                    <div id="1" class="mfp-hide white-popup-block popup-text text-center">
                        <p class="solut">
                            Access to enterprise applications round the clock is crucial for key business
                            processes.
                            Our applications are internally developed; from CRM and ERP systems as well as
                            Business Intelligence, Atlancis has expert
                            teams that can design, implement, manage and support all major applications that
                            support your business.
                        </p>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#applications">
                            Learn More <i class="fas fa-angle-double-right"></i></button>
                        <div class="clearfix"></div>
                        <br/>
                    </div>
                </div>
            </div>
            <!-- End of col -->

        </div>
        <!-- End row -->
    </div>
    <!--end container-->
</section>
<!--FEATURED SOLUTIONS ********************************************************************************************-->

<!--OUR PRODUCTS *********************************************************************************************-->
<section id="our-products" class="ts-block" data-bg-image="{{ asset('front/assets/img/partners2.jpg') }}">
    <div class="container">
        <div class="ts-title text-center">
            <h2 class="pro">Our Products</h2>
        </div>
        <!--end ts-title-->
        <div class="row no-gutters ts-cards-same-height">
            <!--Price Box-->
            <div class="col-sm-4 col-lg-4">
                <div class="card text-center ts-price-box" data-animate="ts-fadeInUp">
                    <div class="card-header p-0">
                        <h5 class="mb-0 py-3 text-white" data-bg-color="#1b1464">Kinetic HCM</h5>
                        <div class="ts-title py-5 mb-0">
                            <h3 class="mb-0 font-weight-normal"><img
                                        src="{{ asset('front/assets/img/team/kinetic.png') }}"
                                        class="img-fluid"></h3>
                            <small class="ts-opacity__50">Human Resource Management System</small>
                        </div>
                    </div>
                    <!--end card-header-->
                    <div class="card-body p-0">
                        <ul class="list-unstyled ts-list-divided">
                            <li>Payroll System</li>
                            <li>Leave Management</li>
                            <li><!--<s class="ts-opacity__50">-->Automated HR Management<!--</s>--></li>
                            <li><!--<s class="ts-opacity__50">-->Highly Efficient<!--</s>--></li>
                        </ul>
                        <!--end list-->
                    </div>
                    <!--end card-body-->
                    <div class="card-footer bg-transparent pt-0 ts-border-none">
                        <a href="#" class="btn btn-outline-primary" data-toggle="modal" data-target="#kinetic">Learn
                            More >></a>
                    </div>
                </div>
                <!--end card-->
            </div>
            <!--end price-box col-md-4-->

            <!--Price Box Promoted-->
            <div class="col-sm-4 col-lg-4">
                <div class="card text-center ts-price-box ts-price-box__promoted" data-animate="ts-fadeInUp"
                     data-delay="0.1s">
                    <div class="card-header p-0">
                        <h5 class="mb-0 py-3 text-white" data-bg-color="#BA2127">Atlancis Cloud</h5>
                        <div class="ts-title text-white py-5 mb-0">
                            <h3 class="mb-0 font-weight-normal">
                                <!--a href="https://cloud.atlancis.com"--><img
                                        src="{{ asset('front/assets/img/team/cloud.png') }}"
                                        class="img-fluid"></a>
                            </h3>
                            <!--a href="https://cloud.atlancis.com"><small class="ts-opacity__50" style="color: #0d47a1 !important;"></small></a-->
                            <br>
                            <small class="ts-opacity__50" style="color: #000000;">Cloud Services</small>
                        </div>
                    </div>
                    <!--end card-header-->
                    <div class="card-body p-0">
                        <ul class="list-unstyled ts-list-divided">
                            <li>Infrastructure as a Service (IaaS)</li>
                            <li>Backup as a Service (BaaS)</li>
                            <li>Bare Metal as a Service (BMaaS)</li>
                            <li>Platform as a Service (PaaS)</li>
                            <li>Software as a Service (SaaS)</li>
                            <li>Security as a Service (SECaaS)</li>
                        </ul>
                        <!--end list-->
                    </div>
                    <!--end card-body-->
                    <div class="card-footer bg-transparent pt-0 ts-border-none">
                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#cloud">Learn More
                            >></a>
                    </div>
                </div>
                <!--end card-->
            </div>
            <!--end price-box col-md-4-->

            <!--Price Box-->
            <div class="col-sm-4 col-lg-4">
                <div class="card text-center ts-price-box" data-animate="ts-fadeInUp" data-delay="0.2s">
                    <div class="card-header p-0">
                        <h5 class="mb-0 py-3 text-white" data-bg-color="#1b1464">iLearn</h5>
                        <div class="ts-title py-5 mb-0">
                            <h3 class="mb-0 font-weight-normal">
                                <img src="{{ asset('front/assets/img/team/iLearn.png') }}" class="img-fluid">
                            </h3>
                            <small class="ts-opacity__50">Learning Management System</small>
                        </div>
                    </div>
                    <!--end card-header-->
                    <div class="card-body p-0">
                        <ul class="list-unstyled ts-list-divided">
                            <li>Dynamic Capability To Plan</li>
                            <li>Deliver And Analyze Learning Progress of Learners</li>
                            <li>Documentation</li>
                            <li>Tracking And Analysis Functions to Meet Learning Outcomes</li>
                        </ul>
                        <!--end list-->
                    </div>
                    <!--end card-body-->
                    <div class="card-footer bg-transparent pt-0 ts-border-none">
                        <a href="#" class="btn btn-outline-primary" data-toggle="modal" data-target="#ilearn">Learn
                            More >></a>
                    </div>
                </div>
                <!--end card-->
            </div>
            <!--end price-box col-md-4-->
        </div>
        <!--end row-->

        <!--end ts-title-->
        <div class="row no-gutters ts-cards-same-height" style="margin-top: 25px;">
            <!--Price Box-->
            <div class="col-sm-4 col-lg-4">
                <div class="card text-center ts-price-box" data-animate="ts-fadeInUp">
                    <div class="card-header p-0">
                        <h5 class="mb-0 py-3 text-white" data-bg-color="#1b1464">Social Light</h5>
                        <div class="ts-title py-5 mb-0">
                            <h3 class="mb-0 font-weight-normal"><img
                                        src="{{ asset('front/assets/img/team/social.png') }}"
                                        class="img-fluid"></h3>
                            <small class="ts-opacity__50">Social Media Analytics</small>
                        </div>
                    </div>
                    <!--end card-header-->
                    <div class="card-body p-0">
                        <ul class="list-unstyled ts-list-divided">
                            <li>Data Specific Analysis</li>
                            <li>Real Time Data Analysis</li>
                            <li><!--<s class="ts-opacity__50">-->Automated Data Capture<!--</s>--></li>
                            <li><!--<s class="ts-opacity__50">-->Graphical Display Analysis<!--</s>--></li>
                        </ul>
                        <!--end list-->
                    </div>
                    <!--end card-body-->
                    <div class="card-footer bg-transparent pt-0 ts-border-none">
                        <a href="#" class="btn btn-outline-primary" data-toggle="modal" data-target="#social">Learn
                            More >></a>
                    </div>
                </div>
                <!--end card-->
            </div>
            <!--end price-box col-md-4-->

            <!--Price Box Promoted-->
            <div class="col-sm-4 col-lg-4">
                <div class="card text-center ts-price-box ts-price-box__promoted" data-animate="ts-fadeInUp"
                     data-delay="0.1s">
                    <div class="card-header p-0">
                        <h5 class="mb-0 py-3 text-white" data-bg-color="#1e8ece">Atlancis Leasing</h5>
                        <div class="ts-title text-white py-5 mb-0">
                            <h3 class="mb-0 font-weight-normal">
                                <img src="{{ asset('front/assets/img/team/leasing.png') }}" class="img-fluid">
                            </h3>
                            <small class="ts-opacity__50" style="color: #000000;">Leasing Services</small>
                        </div>
                    </div>
                    <!--end card-header-->
                    <div class="card-body p-0">
                        <ul class="list-unstyled ts-list-divided">
                            <li>Tax advantages</li>
                            <li>Predictability</li>
                            <li>Lower capital costs</li>
                            <li>No costly upgrades</li>
                            <li>Reducing labor costs</li>
                        </ul>
                        <!--end list-->
                    </div>
                    <!--end card-body-->
                    <div class="card-footer bg-transparent pt-0 ts-border-none">
                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#leasing">Learn
                            More >></a>
                    </div>
                </div>
                <!--end card-->
            </div>
            <!--end price-box col-md-4-->

            <!--Price Box-->
            <div class="col-sm-4 col-lg-4">
                <div class="card text-center ts-price-box" data-animate="ts-fadeInUp" data-delay="0.2s">
                    <div class="card-header p-0">
                        <h5 class="mb-0 py-3 text-white" data-bg-color="#1b1464">iQueue</h5>
                        <div class="ts-title py-5 mb-0">
                            <h3 class="mb-0 font-weight-normal">
                                <img src="{{ asset('front/assets/img/team/iQueue.png') }}" class="img-fluid">
                            </h3>
                            <small class="ts-opacity__50">Queue Management System</small>
                        </div>
                    </div>
                    <!--end card-header-->
                    <div class="card-body p-0">
                        <ul class="list-unstyled ts-list-divided">
                            <li> Improves The Customer Experience</li>
                            <li>Can Be Adapted to Any Venue</li>
                            <li>Incident Management On Capture</li>
                            <li>Optimizing Staff Levels</li>
                        </ul>
                        <!--end list-->
                    </div>
                    <!--end card-body-->
                    <div class="card-footer bg-transparent pt-0 ts-border-none" id="clients">
                        <a href="#" class="btn btn-outline-primary" data-toggle="modal" data-target="#iqueue">Learn
                            More >></a>
                    </div>
                </div>
                <!--end card-->
            </div>
            <!--end price-box col-md-4-->
        </div>
        <!--end row-->
    </div>
    <!--end container-->
</section>
<!--END OF OUR PRODUCTS *****************************************************************************************-->

<!-- CLIENTS IMAGE ********************************************************************************************-->
<section id="img" class="" data-bg-color="#f6f6f6" data-bg-image="{{ asset('front/assets/img/testimonial.jpg') }}">
</section>
<!--END CLIENTS IMAGE ****************************************************************************************-->

<!-- CLIENTS ********************************************************************************************-->
<section id="our-clients" class="ts-block" data-bg-color="#f6f6f6">
    <div class="container">
        <div class="ts-title" style="padding-bottom: 0px; margin-bottom: 0px;">
            <h2>Our Clients</h2>
        </div>
        <div class="d-block justify-content-between align-items-center text-center ts-partners customer-logos">
            <a class="slide" href="#">
                <img src="{{ asset('front/assets/img/clients/Logos_140_1408.jpg') }}" alt="">
            </a>
            <a class="slide" href="#">
                <img src="{{ asset('front/assets/img/clients/Logos_140_1406.jpg') }}" alt="">
            </a>
            <a class="slide" href="#">
                <img src="{{ asset('front/assets/img/clients/Logos_140_1407.jpg') }}" alt="">
            </a>
            <a class="slide" href="#">
                <img src="{{ asset('front/assets/img/clients/Logos_140_1403.jpg') }}" alt="">
            </a>
            <a class="slide" href="#">
                <img src="{{ asset('front/assets/img/clients/Logos_140_1402.jpg') }}" alt="">
            </a>
            <a class="slide" href="#">
                <img src="{{ asset('front/assets/img/clients/Logos_140_14011.jpg') }}" alt="">
            </a>
            <a class="slide" href="#">
                <img src="{{ asset('front/assets/img/clients/Logos_140_14012.jpg') }}" alt="">
            </a>
            <a class="slide" href="#">
                <img src="{{ asset('front/assets/img/clients/Logos_140_14013.jpg') }}" alt="">
            </a>
            <a class="slide" href="#">
                <img src="{{ asset('front/assets/img/clients/Logos_140_14014.jpg') }}" alt="">
            </a>
            <a class="slide" href="#">
                <img src="{{ asset('front/assets/img/clients/Logos_140_1404.jpg') }}" alt="">
            </a>
            <a class="slide" href="#">
                <img src="{{ asset('front/assets/img/clients/Logos_140_14010.jpg') }}" alt="">
            </a>
            <a class="slide" href="#">
                <img src="{{ asset('front/assets/img/clients/Logos_140_140.jpg') }}" alt="">
            </a>
            <a class="slide" href="#">
                <img src="{{ asset('front/assets/img/clients/Logos_140_1405.jpg') }}" alt="">
            </a>
            <a class="slide" href="#">
                <img src="{{ asset('front/assets/img/clients/Logos_140_1409.jpg') }}" alt="">
            </a>
            <a class="slide" href="#">
                <img src="{{ asset('front/assets/img/clients/Logos_140_14015.jpg') }}" alt="">
            </a>
            <a class="slide" href="#">
                <img src="{{ asset('front/assets/img/clients/Logos_140_14016.jpg') }}" alt="">
            </a>

        </div>
        <!--end logos-->

    </div>
</section>
<!--END CLIENTS ****************************************************************************************-->

<!-- CLIENTS TESTIMONIALS ********************************************************************************************-->
<section id="our-clients" class="ts-block">
    <div class="container">
        <div class="ts-title">
            <h2 class="our-clients">Client Testimonials</h2>
        </div>
        <!--end ts-title-->
        <div class="row">
            <div class="col-md-12">
                <div class="owl-carousel ts-carousel-blockquote owl" data-owl-dots="1"
                     data-animate="ts-zoomInShort">
                    <blockquote class="blockquote">
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <!--person image-->
                                <!--<figure>
                                    <aside>
                                        <i class="fa fa-quote-right"></i>
                                    </aside>
                                    <div class="ts-circle__lg"
                                         data-bg-image="assets/img/Avatar_Female.jpg"></div>
                                </figure>-->
                                <!--end person image-->
                                <!--cite-->
                                <p>
                                    We have worked with Atlancis on many occasions with extraordinary success.
                                    They have
                                    outstanding expertise in Enterprise ICT solutions and an extremely good
                                    understanding of commercial requirements which is rare among IT firms. In a
                                    nutshell, they are on top of their game.
                                </p>
                                <!--end cite-->
                                <!--person name-->
                                <footer class="blockquote-footer">
                                    <!--<h4>Jane Doe</h4>-->
                                    <h4 class="text-center">CIO</h4>
                                    <h4 class="text-center">Power & Energy</h4>
                                </footer>
                                <!--end person name-->
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <!--person image-->
                                <!--<figure>
                                    <aside>
                                        <i class="fa fa-quote-right"></i>
                                    </aside>
                                    <div class="ts-circle__lg" data-bg-image="assets/img/Avatar_Male.jpg"></div>
                                </figure>-->
                                <!--end person image-->
                                <!--cite-->
                                <p>
                                    Atlancis delivered to us innovative solutions which enabled us focus on
                                    strategic
                                    initiatives that better serve our clients. With Atlancis we can now focus on
                                    our
                                    customers better.
                                </p>
                                <!--end cite-->
                                <!--person name-->
                                <footer class="blockquote-footer">
                                    <!--<h4>John Doe</h4>-->
                                    <h4 class="text-center">ICT Manager</h4>
                                    <h4 class="text-center">Telecommunications</h4>
                                </footer>
                                <!--end person name-->
                            </div>
                        </div>

                    </blockquote>
                    <!--end blockquote-->
                    <blockquote class="blockquote">
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <!--person image-->
                                <!--<figure>
                                    <aside>
                                        <i class="fa fa-quote-right"></i>
                                    </aside>
                                    <div class="ts-circle__lg"
                                         data-bg-image="assets/img/Avatar_Female.jpg"></div>
                                </figure>-->
                                <!--end person image-->
                                <!--cite-->
                                <p>
                                    We have worked with Atlancis on many occasions with extraordinary success.
                                    They have
                                    outstanding expertise in Enterprise ICT solutions and an extremely good
                                    understanding of commercial requirements which is rare among IT firms. In a
                                    nutshell, they are on top of their game.
                                </p>
                                <!--end cite-->
                                <!--person name-->
                                <footer class="blockquote-footer">
                                    <!--<h4>Jane Doe</h4>-->
                                    <h4 class="text-center">CIO</h4>
                                    <h4 class="text-center">Power & Energy</h4>
                                </footer>
                                <!--end person name-->
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <!--person image-->
                                <!--<figure>
                                    <aside>
                                        <i class="fa fa-quote-right"></i>
                                    </aside>
                                    <div class="ts-circle__lg" data-bg-image="assets/img/Avatar_Male.jpg"></div>
                                </figure>-->
                                <!--end person image-->
                                <!--cite-->
                                <p>
                                    Atlancis delivered to us innovative solutions which enabled us focus on
                                    strategic
                                    initiatives that better serve our clients. With Atlancis we can now focus on
                                    our
                                    customers better.
                                </p>
                                <!--end cite-->
                                <!--person name-->
                                <footer class="blockquote-footer">
                                    <!--<h4>John Doe</h4>-->
                                    <h4 class="text-center">ICT Manager</h4>
                                    <h4 class="text-center">Telecommunications</h4>
                                </footer>
                                <!--end person name-->
                            </div>
                        </div>

                    </blockquote>
                    <!--end blockquote-->
                </div>
            </div>
        </div>

    </div>
</section>
<!--END OUR CLIENTS TESTIMONIALS ********************************************************************************************-->

<!--PRODUCTS GALLERY ********************************************************************************************-->
<section id="gallery" class="ts-block ts-shape-mask__up" data-bg-color="#f7f7f7"
         data-bg-image="assets/img/" data-bg-size="contain" data-bg-repeat="no-repeat">
    <div class="ts-title text-center">
        <h2 class="pro">Products Gallery</h2>
    </div>
    <!--end ts-title-->
    <div class="owl-carousel ts-carousel-centered py-2 mb-5 zoomy" data-owl-loop="1" data-owl-nav="1"
         data-owl-items="3" data-owl-margin="30" data-owl-center="1">
        <div class="slide">
            <img src="{{ asset('front/assets/img/img-screen-desktop.png') }}" class="ts-shadow__md ts-border-radius__md"
                 alt="">
        </div>
        <div class="slide">
            <img src="{{ asset('front/assets/img/img-screen-desktop_2.png') }}"
                 class="ts-shadow__md ts-border-radius__md" alt="">
        </div>
        <div class="slide">
            <img src="{{ asset('front/assets/img/img-screen-desktop%20(1).png') }}"
                 class="ts-shadow__md ts-border-radius__md"
                 alt="">
        </div>
        <div class="slide">
            <img src="{{ asset('front/assets/img/img-screen-desktop_4.png') }}"
                 class="ts-shadow__md ts-border-radius__md" alt="">
        </div>
        <div class="slide">
            <img src="{{ asset('front/assets/img/img-screen-desktop_5.png') }}"
                 class="ts-shadow__md ts-border-radius__md" alt="">
        </div>
        <div class="slide">
            <img src="{{ asset('front/assets/img/img-screen-desktop_6.jpg') }}"
                 class="ts-shadow__md ts-border-radius__md" alt="">
        </div>
        <div class="slide">
            <img src="{{ asset('front/assets/img/img-screen-desktop_7.jpg') }}"
                 class="ts-shadow__md ts-border-radius__md" alt="">
        </div>
    </div>
</section>
<!--end #products gallery.ts-block-->

<!--ATLANCIS ADVANTAGE ********************************************************************************************-->
<section id="organize" class="ts-block mt-5">
    <div class="container">
        <div class="ts-title text-center">
            <h2 class="adv">The Atlancis Advantage!</h2>
        </div>
        <!--end ts-title-->
        <div class="row align-items-center">
            <div class="col-md-4">
                <figure class="text-right ts-xs-text-center" data-animate="ts-fadeInUp">
                    <figure class="icon">
                        <img src="{{ asset('front/assets/img/single.png') }}" class="" alt="">
                    </figure>
                    <h4 class="mb-2">Single Provider, Multiple Solutions</h4>
                    <p>
                        Our solutions tackle numerous business challenges.
                    </p>
                </figure>
                <figure class="text-right ts-xs-text-center" data-animate="ts-fadeInUp" data-delay=".1s">
                    <figure class="icon">
                        <img src="{{ asset('front/assets/img/cost.png') }}" class="img-fluid" alt="">
                    </figure>
                    <h4 class="mb-2">Cost Savings due to Integrated Solutions</h4>
                    <p>
                        Optimize your business without needing to spend more on additional products.
                    </p>
                </figure>
                <figure class="text-right ts-xs-text-center" data-animate="ts-fadeInUp" data-delay=".3s">
                    <figure class="icon">
                        <img src="{{ asset('front/assets/img/use.png') }}" class="img-fluid" alt="">
                    </figure>
                    <h4 class="mb-2">Use What You Need</h4>
                    <p>
                        We design products that allow you to choose only what will solve your business
                        challenge.
                    </p>
                </figure>
            </div>
            <!--end col-md-4-->

            <div class="col-md-4 my-5 d-flex justify-content-center align-items-center">
                <div class="image position-relative">
                    <img src="{{ asset('front/assets/img/img-phone-1st-screen.png') }}" class="mw-100" alt=""
                         data-animate="ts-zoomInShort" data-delay=".1s">
                    <aside class="ts-svg ts-svg__organic-shape-04 ts-background-size-contain"
                           data-animate="ts-zoomInShort" data-delay=".4s"></aside>
                </div>
            </div>
            <!--end col-md-4-->

            <div class="col-md-4">
                <figure class="ts-xs-text-center" data-animate="ts-fadeInUp">
                    <figure class="icon">
                        <img src="{{ asset('front/assets/img/customized.png') }}" class="" alt="">
                    </figure>
                    <h4 class="mb-2">Customised To Your Needs</h4>
                    <p>
                        Our solutions are tailor-made for the specific needs of your business.
                    </p>
                </figure>
                <figure class="ts-xs-text-center" data-animate="ts-fadeInUp" data-delay=".1s">
                    <figure class="icon">
                        <img src="{{ asset('front/assets/img/working.png') }}" class="img-fluid" alt="">
                    </figure>
                    <h4 class="mb-2">Working In Partnership With You</h4>
                    <p>
                        We ensure that your experience from start to finish is flawless.
                    </p>
                </figure>
                <figure class="ts-xs-text-center" data-animate="ts-fadeInUp" data-delay=".3s">
                    <figure class="icon">
                        <img src="{{ asset('front/assets/img/local.png') }}" class="img-fluid" alt="">
                    </figure>
                    <h4 class="mb-2">Local Knowledge</h4>
                    <p>
                        Nothing beats having an understanding of the environment that affects your business.
                    </p>
                </figure>
            </div>
            <!--end col-md-4-->

            <div class="col-md-4 offset-md-4">
                <figure class="ts-xs-text-center" data-animate="ts-fadeInUp">
                    <figure class="icon">
                        <img src="{{ asset('front/assets/img/growth.png') }}" class="img-fluid mx-auto d-block" alt="">
                    </figure>
                    <h4 class="mb-2">Easy Growth Path When You Need More Capacity</h4>
                    <p>
                        Our service solutions' capacity is easily expandable whenever required.
                    </p>
                </figure>
            </div>
            <!--end col-md-4-->
        </div>
        <!--end row-->
    </div>
    <!--end container-->
</section>
<!--end #Atlancis Advantages-->

<!--ATLANCIS SLOGAN ********************************************************************************************-->
<section id="slogan">
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="p-1 text-center ts-border-radius__round-shape ts-shadow__sm"
                     data-bg-color="#1b1464">
                    <div class="bg-white p-5 ts-border-radius__round-shape">
                        <div class="row">
                            <div class="col-md-12">
                                <a href="#">
                                    <img src="{{ asset('front/assets/img/Atlancis%20Slogan.png') }}" class="mw-100 py-3"
                                         alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr class="position-absolute ts-top__0 ts-bottom__0 m-auto w-100 ts-z-index__-1">
</section>
<!--END ATLANCIS SLOGAN ********************************************************************************************-->

<!--SUBSCRIBE *******************************************************************************************-->
<section id="subscribe" class="ts-block ts-separate-bg-element"
         data-bg-image="{{ asset('front/assets/img/bg-pattern-dot.png') }}"
         data-bg-size="inherit" data-bg-image-opacity=".1" data-bg-repeat="repeat">
    <div class="container">
        <h3>Subscribe To Our News Letter!</h3>


        <form class="ts-form ts-form-email ts-labels-inside-input" method="POST"
              data-php-path="assets/php/email.php" id="subscribe">
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group mb-0">
                        <label for="email-subscribe">Email address</label>
                        <input type="email" class="form-control" id="email-subscribe"
                               aria-describedby="subscribe" name="email" placeholder="" required>
                        <small class="form-text mt-2 ts-opacity__50">*You’ll get only relevant news</small>
                    </div>
                    <!--end form-group-->
                </div>
                <!--end col-md-10-->
                <div class="col-md-2">
                    <button type="submit" class="btn btn-primary w-100" name="scribe">Submit >></button>
                </div>
                <!--end col-md-2-->
            </div>
            <!--end row-->
        </form>
        <!--end ts-form-->
    </div>
    <!--end container-->
</section>
<!--END SUBSCRIBE ***************************************************************************************-->

<!--NUMBERS *********************************************************************************************-->
<section id="numbers" class="ts-block ts-separate-bg-element"
         data-bg-image="{{ asset('front/assets/img/pattern-topo.png') }}"
         data-bg-image-opacity=".05" data-bg-size="inherit" data-bg-repeat="repeat">
    <div class="container-fluid">
        <div class="ts-promo-numbers">
            <div class="ts-promo-number text-center">
                <h2 class="loc" data-animate="ts-zoomIn">Our Current Locations</h2>
                <img src="assets/img/mapArtboard%201%20(3).svg" class="img-fluid svg"
                     style="width: 100% \9; height: 750px;">
                <span class="ts-promo-number-divider"></span>
            </div>
            <!--end ts-promo-number-->
        </div>
        <!--end ts-promo-numbers-->
    </div>
    <!--end container-->
</section>
<!--END NUMBERS *****************************************************************************************-->

</main>
<!--end #content-->


<!--*********************************************************************************************************-->
<!--************ FOOTER *************************************************************************************-->
<!--*********************************************************************************************************-->
<footer id="ts-footer">

    <div class="map ts-height__400px" id="map"></div>

    <section id="contact" class="ts-separate-bg-element" data-bg-image="{{ asset('front/assets/img/3.jpg') }}"
             data-bg-image-opacity=".1"
             data-bg-color="#173158">
        <div class="container">
            <div class="ts-box mb-0 p-5 ts-mt__n-10">
                <div class="row">
                    <div class="col-md-4">
                        <h3>Contact Us</h3>
                        <address>
                            <figure>
                                <i class="fas fa-location-arrow"></i>
                                Atlancis Technologies Limited, 5th Floor, Top Plaza Kindaruma Rd,
                                Off Ngong Rd, Kilimani, Nairobi, Kenya.
                            </figure>
                            <br>
                            <figure>
                                <div class="font-weight-bold"><i class="fas fa-envelope"></i> Email:</div>
                                <a href="mailto:info@atlancis.com" style="color: #1e8ece;">info@atlancis.com</a>
                            </figure>
                            <figure>
                                <div class="font-weight-bold"><i class="fas fa-phone"></i> Phone:</div>
                                <a href="tel:+254-(0)20-5132100" style="color: #1e8ece;">+254-(0)20-5132100</a>
                            </figure>
                            <!--<div class="font-weight-bold"><i class="fab fa-skype"></i> Skype:</div>
                            atlancis-->
                            <ul class="list-inline partnerimg">
                                <li class="list-inline-item"><a class="social"
                                                                href="https://www.facebook.com/Atlancis/"><i
                                                class="fab fa-facebook-square"></i></a></li>
                                <li class="list-inline-item"><a class="social"
                                                                href="https://twitter.com/atlancis_tech?lang=en"><i
                                                class="fab fa-twitter-square"></i></a></li>
                                <li class="list-inline-item"><a class="social" href="#"><i class="fab fa-instagram"></i></a>
                                </li>
                                <li class="list-inline-item"><a class="social"
                                                                href="https://ke.linkedin.com/company/africa-neurotech-systems"><i
                                                class="fab fa-linkedin"></i></a>
                                </li>
                                <li class="list-inline-item"><a class="social" href="#"><i
                                                class="fab fa-youtube"></i></a></li>
                            </ul>
                        </address>
                        <!--end address-->
                    </div>
                    <!--end col-md-4-->
                    <div class="col-md-8" id="contact">
                        <h3>Contact Form</h3>

                        <p class="alert alert-default">Kindly note, we are capturing your information for communication
                            convenience.</p>


                        <form id="form-contact" method="POST"
                              class="clearfix ts-form ts-form-email ts-inputs__transparent"
                              action="https://www.atlancis.com/index.php"
                              enctype="multipart/form-data" data-php-path="assets/php/email.html">
                            <!--<div id="valid-msg" class="hide text-center alert alert-success alert-dismissible text-success">
                                Valid Phone Number
                                <button type = "button" class="close" data-dismiss = "alert"><i class="fas fa-times-circle"></i></button>
                            </div>-->
                            <div id="error-msg"
                                 class="hide text-center alert alert-warning alert-dismissible text-warning">
                                Invalid Phone Number
                                <button type="button" class="close" data-dismiss="alert"><i
                                            class="fas fa-times-circle"></i></button>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label for="form-contact-name">Your Name <span
                                                    class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="form-contact-name" name="name"
                                               placeholder="Your Name" required>
                                    </div>
                                    <!--end form-group -->
                                </div>
                                <!--end col-md-6 col-sm-6 -->
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label for="form-contact-email">Your Email <span
                                                    class="text-danger">*</span></label>
                                        <input type="email" class="form-control" id="form-contact-email" name="email"
                                               placeholder="Your Email" required>
                                    </div>
                                    <!--end form-group -->
                                </div>
                                <!--end col-md-6 col-sm-6 -->
                            </div>
                            <!--end row -->
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label for="mobile">Your Mobile Number <span
                                                    class="text-danger">*</span></label>
                                        <input type="tel" class="form-control" id="phone" name="phone" required>
                                        <span id="valid-msg" class="hide text-success">Valid Number</span>
                                        <!--<span id="error-msg" class="hide">Invalid number</span>-->
                                        <input type="hidden" class="form-control" id="phone2" name="phone2">
                                    </div>
                                    <!--end form-group -->
                                </div>
                                <!--end col-md-6 col-sm-6 -->
                            </div>
                            <!--end row -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="form-contact-message">Your Message <span
                                                    class="text-danger">*</span></label>
                                        <textarea class="form-control" id="form-contact-message" rows="5" name="message"
                                                  placeholder="Your Message" required></textarea>
                                    </div>
                                    <!--end form-group -->
                                </div>
                                <!--end col-md-12 -->
                            </div>
                            <!--end row -->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="g-recaptcha"
                                             data-sitekey="6Lf7E2gUAAAAAM4U5FxHHDL293LuT2goFJ_zOgan"></div>
                                    </div>
                                    <!--end form-group -->
                                </div>
                                <!--end col-md-12 -->
                            </div>
                            <!--end row -->
                            <div class="form-group clearfix">
                                <button type="submit" class="btn btn-primary float-right" id="form-contact-submit"
                                        name="send">Send a Message >>
                                </button>
                            </div>
                            <!--end form-group -->
                            <div class="form-contact-status"></div>
                        </form>
                        <!--end form-contact -->
                    </div>
                </div>
                <!--end row-->
            </div>
            <!--end ts-box-->
            <div class="text-center text-white py-4">
                <small>© {{ date('Y') }} Atlancis Technologies Limited, All Rights Reserved.</small>
            </div>
        </div>
        <!--end container-->
    </section>

</footer>
<!--end #footer-->
<!--end page-->

<!--Back to top-->
<div id="back-top" class="back-top"><a href="#top"><i class="fa fa-angle-up" aria-hidden="true"></i> </a></div>
<!--/Back to top-->

<script>
    if (document.getElementsByClassName("ts-full-screen").length) {
        document.getElementsByClassName("ts-full-screen")[0].style.height = window.innerHeight + "px";
    }
</script>
{{--<script src="{{ asset('front/assets/js/jquery-3.3.1.min.js') }}"></script>--}}
{{--<script src="{{ asset('front/assets/js/popper.min.js') }}"></script>--}}
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('front/assets/js/imagesloaded.pkgd.min.js') }}"></script>

<!--Google map-->
<script>
    function myMap() {
        var myCenter = new google.maps.LatLng(-1.29742, 36.79331);
        var mapProp = {
            center: myCenter,
            zoom: 17,
            scrollwheel: false,
            draggable: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map"), mapProp);
        var marker = new google.maps.Marker({position: myCenter});
        marker.setMap(map);
    }
</script>
<!-- End of Google Map -->

<script>
    // Scroll To Top
    $(document).ready(function () {
        // Show or hide the sticky footer button
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('.back-top').fadeIn();
            } else {
                $('.back-top').fadeOut();
            }
        });

        // Animate the scroll to top
        $('.back-top').click(function (event) {
            event.preventDefault();

            $('html, body').animate({scrollTop: 0}, 2000);
        })
    });
</script>


<script>

    $("#phone").mouseleave(function () {
        var tel = $("#phone").intlTelInput("getNumber");
        $("#phone2").val(tel);
        // alert(tel);
    });

    $("#form-contact-submit").click(function () {
        if ($("#phone2").val() == '') {
            window.location.hash = "#contact";
            return false;
        }

    });

</script>

{{--<script src="../cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/intlTelInput.js"></script>--}}

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDNBFQAf3XP93WpzSouuOuFImGCADOOAv8&amp;callback=myMap"></script>
<script src="{{ asset('front/assets/js/isInViewport.jquery.js') }}"></script>
<script src="{{ asset('front/assets/js/jquery.particleground.min.js') }}"></script>
<script src="{{ asset('front/assets/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('front/assets/js/scrolla.jquery.min.js') }}"></script>
<script src="{{ asset('front/assets/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('front/assets/js/jquery-validate.bootstrap-tooltip.min.js') }}"></script>
<script src="cdnjs.cloudflare.com/ajax/libs/gsap/1.20.4/TweenMax.min.js"></script>
<script src="{{ asset('front/assets/js/jquery.wavify.js') }}"></script>
<script src="{{ asset('front/assets/js/custom.js') }}"></script>
<script src="{{ asset('front/assets/js/index.js') }}"></script>
<script src="{{ asset('front/assets/js/slick.js') }}"></script>
<script src="{{ asset('front/assets/js/slide.js') }}"></script>
<script src="{{ asset('front/assets/js/modclose.js') }}"></script>
<!-- Pure Chat -->)
<!--<script type='text/javascript' data-cfasync='false'>window.purechatApi = {
        l: [], t: [], on: function () {
            this.l.push(arguments);
        }
    };
    (function () {
        var done = false;
        var script = document.createElement('script');
        script.async = true;
        script.type = 'text/javascript';
        script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript';
        document.getElementsByTagName('HEAD').item(0).appendChild(script);
        script.onreadystatechange = script.onload = function (e) {
            if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
                var w = new PCWidget({c: 'e59fc53a-17d3-433f-92a1-6ed807649ba5', f: true});
                done = true;
            }
        };
    })();</script>-->
<!-- End of Pure Chat -->

<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
    (function () {
        var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/5b6ad9bfdf040c3e9e0c672c/default';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();
</script>
<!--End of Tawk.to Script-->
</body>

</html>