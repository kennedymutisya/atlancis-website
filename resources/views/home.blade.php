@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row ">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Dashboard
                        <span id='ct' class="badge badge-info float-right"></span>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="float-right pb-3">
                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#exampleModal">
                                        <i class="fa fa-plus"> Add New Item</i>
                                    </button>

                                    <!-- Modal -->
                                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Add Item</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <createitem></createitem>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <items :allitems="{{ \App\Item::all() }}"></items>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container pt-9 mt-9">
        <div class="row">
            <table class="table table-condensed table-hover">
                <thead>
                <tr>
                    <th>Progress</th>
                    <th></th>
                    {{--<th>Last Activity</th>--}}
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Add an Item</td>
                    <td><i class="fa fa-check"></i></td>
                </tr>
                <tr>
                    <td>Calculate prices</td>
                    <td><i class="fa fa-check"></i></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
